#!/usr/bin/env perl
use v5.14.0;
use strict;
use warnings;

use Cwd;

BEGIN {
    unshift @INC, getcwd() . '/lib';
}

use AshTsk::DataSource;
use AshTsk::Loader;
use AshTsk::Parser;

my $begin_time = time;

my $source = AshTsk::DataSource->new('./data/words.txt' );
my $words =  $source->read();

my $dl = AshTsk::Loader->new($words, 5, AshTsk::Parser->new);
my $res = $dl->process();

say "done!";
say "Processed words ".$res->{total_count};
say "Success ".$res->{success_count};
say "Fail ".$res->{fail_count};
say "Total time ". ( time - $begin_time ) . ' sec';