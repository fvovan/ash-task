package AshTsk::Loader;

use strict;
use warnings;

use Carp qw(croak);

use AnyEvent;
use AnyEvent::HTTP;
use AnyEvent::DBI;
use AnyEvent::TLS;

use URI::Escape;

use Data::Dumper;
use Try::Tiny;

sub new {
     my ($class, $words, $max_connections, $parser) = @_;
     
     croak('argument must be arrayref') if (ref $words ne 'ARRAY');
     
     my $self = {
        words => $words,
        dbh   => AnyEvent::DBI->new("DBI:mysql:dbname=ash;host=localhost", "root", "vovan",
           on_connect => sub { shift->exec("SET NAMES UTF8", sub { log_debug("DB connect established"); } ) ;} ,
           on_error => sub { warn $@ ; die; }
        ),
        parser => $parser, 
        max_connections => $max_connections,
     };
     
     bless $self, $class;
     return $self;
}

sub process {
    my ($self) = @_;
    
    my $dbh = $self->{dbh};
     
    my $free_connections = $self->{max_connections}; #parallels
    
    my $cv = AnyEvent->condvar;
    
    my $tls_ctx = AnyEvent::TLS->new( verify => 0 );
    
    my $total_count = scalar(@{$self->{words}});
    my $success_count = 0;
    my $fail_count = 0;
    
    $cv->begin;
    
    my $timer_w; $timer_w = AnyEvent->timer (after => 0, interval => 0.4, cb => sub {
    
        if (! scalar(@{$self->{words}}) ) {
            log_debug('Array empty.. exiting timer');
        
            undef $timer_w;
            $cv->end;
            return;
        }
    
        if ($free_connections) {
            my $word = pop @{$self->{words}};
            log_debug("We have $free_connections concts. Process $word word");
        
            $cv->begin;
            
            http_get prepare_url($word),  req_params($tls_ctx), sub {
                my ($body, $hdr) = @_;
               # warn Dumper($body);
                 
                $free_connections += 1;

                if ($hdr->{Status} =~ /^2/) {
                    log_debug("Good status for $word");
                    
                    my $parsed_data = [];
                    
                    try {                    
                        $parsed_data = $self->parse_page($body);
                    }
                    catch {
                        #open my $err, '>' , './data/log/'.$word.'.txt' or die ('cant open file');
                        #print $err $body;
                        #close $err;
                        
                        log_debug("Parsing Error in word $word");
                    }; 

                    my $results_size = scalar @{$parsed_data};
                    
                    if ($results_size) {
                        $success_count +=1;
                        $dbh->exec("INSERT INTO `query` (word) VALUES (?) ", $word, sub {
                            my ($dbh) = @_;
                           
                            $dbh->exec("SELECT  LAST_INSERT_ID() ; ", sub {
                                my ($dbh, $rows) = @_;
                                my $query_id = $rows->[0][0];
                                
                                #warn Dumper($query_id);
                                
                                foreach my $result (@{$parsed_data}) {
                                    $dbh->exec("INSERT INTO `result` (`query_id`, `url`, `title`, `text`, `position`) 
                                        VALUES (?, ?, ?, ? , ?) ", $query_id, $result->{href}, $result->{title}, $result->{text}, $result->{position},
                                        sub {
                                            $results_size -= 1;
                                            if (!$results_size) {
                                                $cv->end;
                                            }
                                    });
                                }
                            }); 
                        });
                    }
                    else {
                        $fail_count += 1;
                        $cv->end;
                    }   
                } 
                else {
                   log_debug("Bad status for $word ");
                   $fail_count += 1;
                   $cv->end;
                }
            }; 

            $free_connections -= 1;        
        }
        else {
            log_debug('Wait for free concts');
        }
     });
    
    $cv->recv;

    {
        'total_count' => $total_count,
        'success_count' => $success_count,
        'fail_count' => $fail_count,
    }
}

sub prepare_url {
    my $word = shift;
    
    'https://www.google.com/search?q='.uri_escape($word).'&ie=utf-8&oe=utf-8';
}

my $user_agents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
   # 'Mozilla/5.0 (Windows; U; Windows NT 5.1) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/0.4.154.25 Safari/525.19',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
];

sub req_params {
    my ($tls_ctx) = @_;

    %{{
        cookie_jar => {},
        headers    => {
            'User-Agent' => $user_agents->[int rand @$user_agents],
            'Accept-Language' => 'ru-RU,ru;q=0.8,en;q=0.3',
        },
        tls_ctx => $tls_ctx, 
    }};
}

sub parse_page {
    my ($self, $page) = @_;

    return $self->{parser}->parse($page);
 }

sub log_debug {
    warn shift;
}

1;