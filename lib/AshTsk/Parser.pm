package AshTsk::Parser;

use strict;
use warnings;

use Mojo::DOM;
use Data::Dumper;

sub new {
     my ($class) = @_;
          
     my $self = {};
     
     bless $self, $class;
     return $self;
}

sub parse {
    my ($self, $body ) =  @_;
    
    my $dom = Mojo::DOM->new($body);
    
    my $blocks = $dom->find('div#search div#ires ol div.srg');
    
    my $flatten_li = $blocks->map( sub { shift->find('li.g') } )->flatten;
    
    my $counter = 1;
    
    my @ret;
    
    $flatten_li->each( sub {
        my $el = shift;
        
        my $link_el = $el->find('h3.r a')->[0];
        my $text_el = $el->find('div.s span.st')->[0];
        
        push @ret, {
          title =>  ($link_el ? $link_el->text : ''),
          href   => ($link_el ? $link_el->attr('href') : ''),
          position => $counter++,
          text     => ($text_el ? $text_el->text : ''),
        };
        
    } );
    
    #warn ( Dumper([@ret]) );

    [@ret];
}


1;