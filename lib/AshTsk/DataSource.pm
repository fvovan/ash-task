﻿package AshTsk::DataSource;
use strict;
use warnings;

use Carp qw(croak);


sub new {
     my ($class, $filename) = @_;
     my $self = {
        filename => $filename,
     };
     bless $self, $class;
     return $self;
}

sub read {
    my ($self) = @_;
    
    open(my $fh , '<', $self->{filename} ) or croak("cant read file ".$self->{filename});

    my @ret;
    while ( my $url = <$fh>) {
        chomp($url);
        push @ret, $url;
    }
    
    close $fh;

    [ @ret ];    
}

1;